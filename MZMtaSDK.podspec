#
#  Be sure to run `pod spec lint MZMtaSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "MZMtaSDK"
  s.version      = "0.0.2"
  s.summary      = "MZMtaSDK 是基于MTA的统计工具."
  s.homepage     = 'https://git.oschina.net/Gogal'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'kingly09' => 'kingly09@gmail.com' }
  s.source       = { :git => 'https://git.oschina.net/Gogal/MZMtaSDK.git', :tag => s.version.to_s }
  s.platform     = :ios, "8.0"

  s.source_files  = 'sdk/Source/*.{h,m}'
  #s.exclude_files = "sdk/Source/*.h"

  s.libraries  = "z","sqlite3"
  s.vendored_libraries = 'sdk/Source/*.a'
  s.frameworks = "QuartzCore",
                 "Security",
                 "CFNetwork",
                 "SystemConfiguration",
                 "CoreTelephony",
                 "UIKit",
                 "Foundation",
                 "CoreGraphics"

  s.requires_arc = true


end
