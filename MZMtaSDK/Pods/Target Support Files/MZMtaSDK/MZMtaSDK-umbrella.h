#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "MTA.h"
#import "MTAConfig.h"
#import "MTACrashReporter.h"
#import "MZMtaHelper.h"

FOUNDATION_EXPORT double MZMtaSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char MZMtaSDKVersionString[];

