//
//  MZMtaHelper.h
//  MZGogalApp
//
//  Created by li on 16/2/24.
//  Copyright © 2016年 美知互动科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTA.h"
#import "MTAConfig.h"

@interface MZMtaHelper : NSObject

+ (void) setInitSDk;

+(MZMtaHelper *)share;

@end
