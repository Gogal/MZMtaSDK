//
//  MZMtaHelper.m
//  MZGogalApp
//
//  Created by li on 16/2/24.
//  Copyright © 2016年 美知互动科技. All rights reserved.
//

#import "MZMtaHelper.h"

#define K_MTA_APP_KEY @"IJ4AXE3WL76R"

@implementation MZMtaHelper

static MZMtaHelper *gShared = nil;

+(MZMtaHelper *)share
{
    @synchronized(self)
    {
        if (gShared == nil)
            gShared = [[MZMtaHelper alloc] init];
    }
    return gShared;
}

- (id)init
{
    self = [super init];
    if (self)
    {
    
        
    }
    return  self;
}

+ (void) setInitSDk{
    
   [[MTAConfig getInstance] setDebugEnable:TRUE];
    
    
    //[[MTAConfig getInstance] setCustomerUserID:@"1234"];
    
    //[[MTAConfig getInstance] setMaxReportEventLength:1280];
    
    //Old Appkey "DemoApp@MTA"   IG4BJ2YGZ14F
    //[MTA startWithAppkey:@"IG4BJ2YGZ14F"];
    
    //[[MTAConfig getInstance] setCustomerUserID:@"1234"];
    
    //自定义ifa
    //    [[MTAConfig getInstance] setIfa:@"myIfa"];
    
    //push服务的deviceToken
    //    [[MTAConfig getInstance] setPushDeviceToken:@"myXGDeviceToken"];
    
    //[[MTAConfig getInstance] setReportStrategy:MTA_STRATEGY_BATCH];
    
    //[[MTAConfig getInstance] setSmartReporting:FALSE];
    
    //自定义crash处理函数，可以获取到mta生成的crash信息
    //    void (^errorCallback)(NSString *) = ^(NSString * errorString)
    //    {
    //        NSLog(@"error_callback %@",errorString);
    //    };
    //    [[MTAConfig getInstance] setCrashCallback: errorCallback];
    
    [[MTAConfig getInstance] setAutoExceptionCaught:FALSE];
    
    [MTA startWithAppkey:K_MTA_APP_KEY checkedSdkVersion:MTA_SDK_VERSION];
    
    //[MTA reportQQ:@"5059175"];
    
    //    [MTA reportAccount:@"5059175" type:1 ext:@"test"];
    /*
     if(![MTA startWithAppkey:@"I8S27BWQ6HYL" checkedSdkVersion:MTA_SDK_VERSION]){
     //handle exception
     }
     */
    
    
    //[MTA startWithAppkey:@"DemoApp@MTA" checkedSdkVersion:@"0.9.0"];
    //[MTA startWithAppkey:@"DemoApp@MTA"];
    //[MTA trackGameUser:@"g123" world:@"sz1" level:@"10"];
    
    //[MTA trackError:@"I'm error"];
    
    
    
}


@end
